PROGRAM test_io

  USE MPI
  USE H5LT
  USE timing
  USE io

  IMPLICIT NONE


  !________________________________________

  CALL timer_create("Write in double precision",tdouble)
  CALL timer_create("Write in single precision",tsingle)

  ! Init MPI
  !-----------
  CALL MPI_INIT ( error )
  CALL MPI_COMM_SIZE ( MPI_COMM_WORLD, proc_number, error )
  CALL MPI_COMM_RANK ( MPI_COMM_WORLD, my_rank, error )


  ! Create Cartesian grid
  !----------------------
  CALL MPI_Cart_create(MPI_COMM_WORLD,3,(/nblockx,nblocky,nblockz/),(/ .TRUE., .TRUE., .TRUE. /),.TRUE.,cart_comm,error)
  CALL MPI_Comm_rank (cart_comm, my_cart_rank, error)
  CALL MPI_Cart_coords(cart_comm,my_cart_rank,3,my_coordinates,error)

  my_mx = mx / nblockx
  my_my = my / nblocky
  my_mz = mz / nblockz

  ALLOCATE(testdata(my_mx,my_my,my_mz))

  CALL RANDOM_SEED()
  CALL RANDOM_NUMBER(testdata)
  
  CALL write_PHDF5()

  DEALLOCATE(testdata)

  IF (my_rank .EQ. 0) CALL timer_report(0)

  CALL MPI_Finalize (error)

END PROGRAM test_io
